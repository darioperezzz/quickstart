@extends('layouts.app')

@section('content')

	<!-- Bootstrap Boilerplate... -->
	
	<div class="well">
		<!-- Display Validation Errors -->
		@include('common.errors')
    
		<form action="{{ url('task') }}" method="POST">
			{{ csrf_field() }}

			<!-- Task Name -->
			<div class="input-group">
        <input type="text" name="name" id="task-name" class="form-control">
        <button type="submit" class="btn btn-primary">
          Add Task
        </button>
      </div>
		</form>

    @if (count($tasks) > 0)
      <div class="panel panel-default">
        <div class="panel-heading">
          Current Tasks
        </div>
        <div class="panel-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Task</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($tasks as $task)
                <tr>
                  <td class="table-text">
                    <div>{{ $task->name }}</div>
                  </td>
                  <td>
                    <form action="{{ url('task/'.$task->id) }}" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <button class="btn btn-danger">
                        Delete
                      </button>
                    </form>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    @endif
	</div>
@endsection