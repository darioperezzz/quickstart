<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel Quickstart - Basic</title>
	<!-- CSS And Javascript -->
	<link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
</head>
<body>
	<div class="container">
		@yield('content')
	</div>
</body>
</html>